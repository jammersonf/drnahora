import Cleave from 'cleave.js'
import IMask from 'imask'
//require('cleave.js/dist/addons/cleave-phone.br');

export default {
	bind: function (el, binding) {
		el.mask = new IMask(el, { mask: '(00) 0000-0000' })
	},
	unbind: function (el) {
		el.mask.destroy()
	}
}
