import numeral from 'numeral'

export default function (value) {
	return numeral(value).format('R$0.00')
}
