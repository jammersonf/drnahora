<?php namespace Gogo\Blog\Tests;

use PluginTestCase;
use Gogo\Blog\Models\Category;

class CategoryTest extends PluginTestCase
{
	public function setup(){
		parent::setup();

		include plugins_path('gogo/blog/routes.php');
	}

	public function testCategoryList()
	{
		$response = $this->json('GET', '/v1/blog/categories');
		$response->assertStatus(200);
		$response->assertJsonStructure(['total', 'data', 'per_page']);
	}
}
