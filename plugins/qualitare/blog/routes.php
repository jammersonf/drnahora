<?php

Route::group(['prefix' => 'v1'], function () {
	Route::group(['prefix' => 'blog'], function () {
		Route::group(['prefix' => 'posts'], function () {
			Route::get('/search/{keyword}', ['uses' => 'Qualitare\Blog\Controllers\Posts@search']);
			Route::get('/category/{slug}', ['uses' => 'Qualitare\Blog\Controllers\Posts@list']);
			Route::get('/featured/{slug}', ['uses' => 'Qualitare\Blog\Controllers\Posts@featured']);
			Route::get('/{id}', ['uses' => 'Qualitare\Blog\Controllers\Posts@show']);
		});

		Route::group(['prefix' => 'categories'], function () {
			Route::get('/', ['uses' => 'Qualitare\Blog\Controllers\Categories@list']);
		});
 	});
});
