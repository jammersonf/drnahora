<?php namespace Qualitare\Drnahora\Models;

use Model;
use Qualitare\Drnahora\Models\Shosp;

/**
 * Model
 */
class Unidades extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    use \October\Rain\Database\Traits\SoftDelete;

    protected $dates = ['deleted_at'];


    /**
     * @var string The database table used by the model.
     */
    public $table = 'qualitare_drnahora_unidades';

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];

    public $attachOne = ['imagem' => 'System\Models\File'];

    public $belongsToMany = [
        'medicos' => [
            'Qualitare\Drnahora\Models\Medicos',
            'table' => 'qualitare_drnahora_medico_unidades'
        ],
    ];

    public function getCodigoShospOptions(){
        $shosp = new Shosp();
        $unidades = [];
        foreach($shosp->getUnidades() as $m){
            $unidades[$m['codigoUnidade']] =  $m['nome'];
        }

        return $unidades;
    }
}
