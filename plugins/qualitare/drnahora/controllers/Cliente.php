<?php namespace Qualitare\Drnahora\Controllers;

use Backend\Classes\Controller;
use BackendMenu;
use http\Env\Response;
use Illuminate\Http\Request;
use Qualitare\Drnahora\Models\Clientes;
use Qualitare\Drnahora\Models\Shosp;

class Cliente extends Controller
{
    public $implement = [        'Backend\Behaviors\ListController',        'Backend\Behaviors\FormController',        'Backend\Behaviors\ReorderController'    ];
    
    public $listConfig = 'config_list.yaml';
    public $formConfig = 'config_form.yaml';
    public $reorderConfig = 'config_reorder.yaml';

    public function __construct()
    {
        parent::__construct();
    }

    public function create(Request $request){
            $input = $request->all();
            $shosp = new Shosp();
            $clientes = new Clientes();
            $shospCustomers = $shosp->getPaciente($input['nome']);
            if($shospCustomers && count($shospCustomers['dados']) > 0){
                foreach($shospCustomers['dados'] as $sc){
                    if($sc['email'] == $input['email']){
                        $cliente = $clientes->where('email', $input['email'])->get();
                        if(count($cliente) > 0){
                            return response(['success' => false], 400);
                        }else{
                            $return = Clientes::create([
                                'nome' => $sc['nome'],
                                'email' => $sc['email'],
                                'telefone' => $sc['telefone'] == '' ? $input['telefone'] : $sc['telefone'],
                                'senha' =>  md5($input['senha']),
                                'codigo_shosp' => $sc['prontuario'],
                                'cpf' => $sc['cpf']
                            ]);
                            return response(['success' => true], 200);
                        }
                    }
                }
            }else{
               $retornoShosp = $shosp->createPaciente($input);
               $return = Clientes::create([
                   'nome' => $retornoShosp['dados']['nome'],
                   'email' => $retornoShosp['dados']['email'],
                   'telefone' => $retornoShosp['dados']['telefone'],
                   'senha' =>  md5($input['senha']),
                   'codigo_shosp' => $retornoShosp['dados']['prontuario'],
                   'cpf' => $retornoShosp['dados']['cpf']
               ]);
               dd($return);
            }
    }
}
