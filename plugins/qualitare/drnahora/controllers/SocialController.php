<?php
namespace Qualitare\DrNaHora\Controllers;

use Backend\Classes\Controller;
use Illuminate\Support\Str;
use Laravel\Socialite\Facades\Socialite;
use \Qualitare\Drnahora\Models\Paciente;
use \Qualitare\Drnahora\Models\Shosp;
use RainLab\User\Facades\Auth;
use RainLab\User\Models\User;

class SocialController extends Controller {

    public function redirect($provider) {
        return Socialite::driver($provider)->stateless()->redirect();
    }

    public function callback($provider) {

        try {

            $getInfo = Socialite::driver($provider)->stateless()->user();

            if ( $provider == 'facebook' ) {

                $user = User::where('email', $getInfo->email)->first();

                if ( $user ) {

                    $user->facebook_id = $getInfo->id;
                    $user->facebook_token = $getInfo->token;

                    $user->save();

                    return redirect('/#/auth/facebook/callback?token='.$getInfo->token);

                } else {
                    try {

                        if (env('APP_ENV') == 'production') {
                            $shosp = new Shosp();

                            // Create registry on shosp api
                            $response = $shosp->createPaciente($getInfo->name, $getInfo->email, '99 99999-9999');

                            if (isset($response['dados'])) {
                                $id = $response['dados']['prontuario'];
                            } else {
                                throw new \Exception('Paciente ja cadastrado.');
                            }
                        } else {
                            $id = 9104283;
                        }

                        $passwd = Str::random(32);

                        // Create user
                        $user = Auth::register([
                            'name' => $getInfo->name,
                            'email' => $getInfo->email,
                            'password' => $passwd,
                            'password_confirmation' => $passwd,
                        ], true);

                        // Update shosp code
                        $user->codigo_shosp = $id;
                        $user->facebook_id = $getInfo->id;
                        $user->facebook_token = $getInfo->token;
                        $user->save();

                        $paciente = new Paciente();
                        $paciente->user_id = $user->id;
                        $paciente->nome = $user->name;
                        $paciente->codigo_shosp = $id;

                        $paciente->save();

                    } catch (\Exception $e) {
                        throw new \Exception($e->getMessage());
                    }

                    return redirect('/#/auth/facebook/callback?token='.$getInfo->token);
                }

            } elseif ( $provider == 'google' ) {

                $user = User::where('email', $getInfo->email)->first();

                if ( $user ) {

                    $user->google_id = $getInfo->id;
                    $user->google_token = $getInfo->token;

                    $user->save();

                    return redirect('/#/auth/google/callback?token='.$getInfo->token);

                } else {
                    try {

                        if (env('APP_ENV') == 'production') {
                            $shosp = new Shosp();

                            // Create registry on shosp api
                            $response = $shosp->createPaciente($getInfo->name, $getInfo->email, '99 99999-9999');

                            if (isset($response['dados'])) {
                                $id = $response['dados']['prontuario'];
                            } else {
                                throw new \Exception('Paciente ja cadastrado.');
                            }
                        } else {
                            $id = 9104283;
                        }

                        $passwd = Str::random(32);

                        // Create user
                        $user = Auth::register([
                            'name' => $getInfo->name,
                            'email' => $getInfo->email,
                            'password' => $passwd,
                            'password_confirmation' => $passwd,
                        ], true);

                        // Update shosp code
                        $user->codigo_shosp = $id;
                        $user->google_id = $getInfo->id;
                        $user->google_token = $getInfo->token;
                        $user->save();

                        $paciente = new Paciente();
                        $paciente->user_id = $user->id;
                        $paciente->nome = $user->name;
                        $paciente->codigo_shosp = $id;

                        $paciente->save();

                    } catch (\Exception $e) {
                        throw new \Exception($e->getMessage());
                    }

                    return redirect('/#/auth/google/callback?token='.$getInfo->token);
                }

            }
        } catch (\Exception $ex) {
            // return redirect('/#/auth/login');
            echo $ex->getMessage();
        }

    }

    public function index() {

//        $user = Socialite::driver($provider)->user();
//        $user = Socialite::driver($provider)->stateless()->user();

//        return response()->json($user);
        return view('welcome');
    }

}
