<?php namespace Qualitare\Drnahora;

use Illuminate\Support\Facades\Mail;
use System\Classes\PluginBase;
use Event;

class Plugin extends PluginBase
{
	public $require = [
		'RainLab.User'
	];

	public function boot()
	{
		// Extend all backend form usage
		Event::listen('backend.form.extendFields', function($widget) {

			// Only for the User controller
			if (!$widget->getController() instanceof \RainLab\User\Controllers\Users) {
				return;
			}

			// Only for the User model
			if (!$widget->model instanceof \RainLab\User\Models\User) {
				return;
			}

			// Add an extra birthday field
			$widget->addFields([
				'codigo_shosp' => [
					'label'   => 'Código SHOSP',
					'comment' => '',
					'type'    => 'text',
					'disabled' => true
				]
			]);
		});

        Event::listen('qualitare.drnahora.seja_medico', function($lead) {

            Mail::send('qualitare.drnahora::mail.seja_medico', [$lead], function($message) {

                $message->to('alxmedeiros@gmail.com');

            });

        });
	}

	public function registerComponents()
	{
	}

	public function registerSettings()
	{
	}
}
