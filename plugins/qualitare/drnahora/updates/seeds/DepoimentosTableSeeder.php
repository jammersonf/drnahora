<?php namespace Qualitare\Drnahora\Updates\Seeds;

use October\Rain\Database\Updates\Seeder;
use Qualitare\Drnahora\Models\Depoimentos;
use Faker;

class DepoimentosTableSeeder extends Seeder
{

	public function run()
	{
		$faker = Faker\Factory::create();

		for ($i = 1; $i <= 3; $i++) {
			for ($k = 0; $k < 10; $k++) {
				$depoimento = Depoimentos::create([
					'nome' => $faker->sentence(3, true),
					'idade' => $faker->numberBetween(1, 99),
					'profissao' => $faker->sentence(3, true),
					'depoimento' => $faker->text(200),
				]);

                $depoimento->save();
			}
		}
	}
}
