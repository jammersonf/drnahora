<?php namespace Qualitare\Drnahora\Updates\Seeds;

use October\Rain\Database\Updates\Seeder;
use Qualitare\Drnahora\Models\Contadores;
use Faker;

class ContadoresTableSeeder extends Seeder
{

	public function run()
	{
		$faker = Faker\Factory::create();
		    $especialidades = ['Especialidades', 'Médicos', 'Unidades', 'Exames'];
		    foreach($especialidades as $espec){
            $contadores = Contadores::create(
				    [
                        'nome' => $espec,
                        'quantidade' => $faker->numberBetween(1, 200),
				    ]
                );
                $contadores->save();
            }
	}
}
