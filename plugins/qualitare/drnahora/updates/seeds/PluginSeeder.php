<?php

namespace Qualitare\Drnahora\Updates\Seeds;

use October\Rain\Database\Updates\Seeder;

class PluginSeeder extends Seeder {

	public function run() {

        if (env('APP_ENV') != 'testing' && env('APP_ENV') != 'development')
            return;

        $this->call([
			UnidadesTableSeeder::class
		]);

		$this->call([
			DepoimentosTableSeeder::class
		]);

		$this->call([
			SejaMedicoTableSeeder::class
		]);

		$this->call([
			ContadoresTableSeeder::class
		]);
	}

}
