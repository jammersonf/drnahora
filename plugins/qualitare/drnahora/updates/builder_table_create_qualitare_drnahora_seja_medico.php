<?php namespace Qualitare\Drnahora\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateQualitareDrnahoraSejaMedico extends Migration
{
    public function up()
    {
        Schema::create('qualitare_drnahora_seja_medico', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('nome', 150);
            $table->string('email', 50);
            $table->string('telefone', 50);
            $table->text('mensagem');
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('qualitare_drnahora_seja_medico');
    }
}
