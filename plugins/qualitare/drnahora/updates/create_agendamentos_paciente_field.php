<?php namespace Qualitare\DrNaHora\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateAgendamentosPacienteField extends Migration
{
	public function up()
	{
		Schema::table('qualitare_drnahora_agendamentos', function(Blueprint $table) {
			$table->integer('paciente_id')->after('user_id');
		});
	}

	public function down()
	{
		Schema::table('qualitare_drnahora_agendamentos', function(Blueprint $table) {
			$table->dropColumn('paciente_id');
		});
	}
}
