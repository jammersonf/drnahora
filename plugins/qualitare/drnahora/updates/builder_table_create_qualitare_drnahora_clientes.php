<?php namespace Qualitare\Drnahora\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateQualitareDrnahoraClientes extends Migration
{
    public function up()
    {
        Schema::create('qualitare_drnahora_clientes', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->text('nome');
            $table->text('email');
            $table->text('telefone');
            $table->text('senha');
            $table->integer('codigo_shosp');
            $table->integer('cpf')->nullable();
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->timestamp('deleted_at')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('qualitare_drnahora_clientes');
    }
}
