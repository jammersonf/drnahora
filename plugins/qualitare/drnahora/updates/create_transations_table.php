<?php namespace Qualitare\Drnahora\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateTransationsTable extends Migration
{
    public function up()
    {
        Schema::create('qualitare_drnahora_transations', function (Blueprint $table) {
            $table->engine = 'InnoDB';

            $table->string('order_id');
            $table->primary('order_id');
            $table->text('transaction_code')->nullable();
            $table->text('body')->nullable();
            $table->text('notification')->nullable();
            $table->unsignedInteger('payment_status')->default(1);
            $table->string('payable_type');
            $table->integer('payable_id');

            $table->integer('installments')->nullable();

            /**
             * 1	Cartão de crédito: o comprador escolheu pagar a transação com cartão de crédito.
             * 2	Boleto: o comprador optou por pagar com um boleto bancário.
             * 3	Débito online (TEF): o comprador optou por pagar a transação com débito online de algum dos bancos conveniados.
             * 4	Saldo PagSeguro: o comprador optou por pagar a transação utilizando o saldo de sua conta PagSeguro.
             * 5	Oi Paggo *: o comprador escolheu pagar sua transação através de seu celular Oi.
             */
            $table->integer('payment_method_type')->nullable();

            /**
             * 101	Cartão de crédito Visa.
             * 102	Cartão de crédito MasterCard.
             * 103	Cartão de crédito American Express.
             * 104	Cartão de crédito Diners.
             * 105	Cartão de crédito Hipercard.
             * 106	Cartão de crédito Aura.
             * 107	Cartão de crédito Elo.
             * 108	Cartão de crédito PLENOCard.
             * 109	Cartão de crédito PersonalCard.
             * 110	Cartão de crédito JCB.
             * 111	Cartão de crédito Discover.
             * 112	Cartão de crédito BrasilCard.
             * 113	Cartão de crédito FORTBRASIL.
             * 114	Cartão de crédito CARDBAN.
             * 115	Cartão de crédito VALECARD.
             * 116	Cartão de crédito Cabal.
             * 117	Cartão de crédito Mais!.
             * 118	Cartão de crédito Avista.
             * 119	Cartão de crédito GRANDCARD.
             * 120	Cartão de crédito Sorocred.
             * 122	Cartão de crédito Up Policard.
             * 123	Cartão de crédito Banese Card.
             * 201	Boleto Bradesco.
             * 202	Boleto Santander.
             * 301	Débito online Bradesco.
             * 302	Débito online Itaú.
             * 303	Débito online Unibanco.
             * 304	Débito online Banco do Brasil.
             * 305	Débito online Banco Real.
             * 306	Débito online Banrisul.
             * 307	Débito online HSBC.
             * 401	Saldo PagSeguro.
             * 501	Oi Paggo. *
             * 701	Depósito em conta - Banco do Brasil
             */
            $table->integer('payment_method_code')->nullable();

            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('qualitare_drnahora_transations');
    }
}
