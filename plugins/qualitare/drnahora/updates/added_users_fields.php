<?php namespace Qualitare\Drnahora\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class AddedUsersFields extends Migration
{
	public function up()
	{
		Schema::table('users', function($table)
		{
			$table->integer('codigo_shosp')->nullable();
		});
	}

	public function down()
	{
		Schema::table('users', function($table)
		{
			$table->dropColumn('codigo_shosp');
		});
	}
}
