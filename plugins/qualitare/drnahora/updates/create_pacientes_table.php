<?php namespace Qualitare\Drnahora\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreatePacientesTable extends Migration
{
    public function up()
    {
        Schema::create('qualitare_drnahora_pacientes', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');

            $table->integer('user_id');

            $table->string('nome');
            $table->integer('codigo_shosp')->nullable();

            $table->timestamps();
            $table->softDeletes();
        });
    }

    public function down()
    {
        Schema::dropIfExists('qualitare_drnahora_pacientes');
    }
}
