<?php namespace Qualitare\DrNaHora\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateServicosFields extends Migration
{
	public function up()
	{
		Schema::table('qualitare_drnahora_servicos', function(Blueprint $table) {
			$table->integer('especialidade_id')->after('valor');
		});
	}

	public function down()
	{
		Schema::table('qualitare_drnahora_servicos', function(Blueprint $table) {
			$table->dropColumn('especialidade_id');
		});
	}
}
