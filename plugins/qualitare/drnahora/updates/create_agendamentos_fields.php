<?php namespace Qualitare\DrNaHora\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateAgendamentosFields extends Migration
{
	public function up()
	{
		Schema::table('qualitare_drnahora_agendamentos', function(Blueprint $table) {
			$table->integer('unidade_id')->after('servico_id');
			$table->integer('horario_codigo')->after('horario');
		});
	}

	public function down()
	{
		Schema::table('qualitare_drnahora_agendamentos', function(Blueprint $table) {
			$table->dropColumn('unidade_id');
			$table->dropColumn('horario_codigo');
		});
	}
}
