<?php namespace Qualitare\DrNaHora\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class AddMedicosEspcialidadesFields extends Migration
{
	public function up()
	{
		Schema::table('qualitare_drnahora_medicos', function(Blueprint $table) {
            $table->string('rqe', 20)->nullable();
		});
	}

	public function down()
	{
	}
}
