<?php namespace Qualitare\Drnahora\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateQualitareDrnahoraServicoMedicos extends Migration
{
    public function up()
    {
        Schema::create('qualitare_drnahora_servico_medicos', function($table)
        {
            $table->engine = 'InnoDB';
            $table->integer('servicos_id');
            $table->integer('medicos_id');
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('qualitare_drnahora_servico_medicos');
    }
}
