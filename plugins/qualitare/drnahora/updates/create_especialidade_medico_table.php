<?php namespace Qualitare\DrNaHora\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateEspecialidadeMedicoTable extends Migration
{
    public function up()
    {
        Schema::create('qualitare_drnahora_especialidade_medico', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
						$table->string('especialidade_id');
						$table->string('medicos_id');
        });
    }

    public function down()
    {
        Schema::dropIfExists('qualitare_drnahora_especialidade_medico');
    }
}
