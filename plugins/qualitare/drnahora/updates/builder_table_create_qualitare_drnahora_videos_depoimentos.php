<?php namespace Qualitare\Drnahora\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateQualitareDrnahoraVideosDepoimentos extends Migration
{
    public function up()
    {
        Schema::create('qualitare_drnahora_videos_depoimentos', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('titulo', 200);
            $table->string('url', 100);
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->timestamp('deleted_at')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('qualitare_drnahora_videos_depoimentos');
    }
}
