<?php namespace Qualitare\Drnahora\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateQualitareDrnahoraMedicoUnidades extends Migration
{
    public function up()
    {
        Schema::create('qualitare_drnahora_medico_unidades', function($table)
        {
            $table->engine = 'InnoDB';
            $table->integer('medicos_id');
            $table->integer('unidades_id');
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('qualitare_drnahora_medico_unidades');
    }
}
