<?php namespace Qualitare\Drnahora\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class AddedOnlineServiceFields extends Migration
{
    public function up()
    {
        Schema::table('qualitare_drnahora_servicos', function($table)
        {
            $table->tinyInteger('online_service')->nullable()->default(0);
        });
    }

    public function down()
    {
        Schema::table('qualitare_drnahora_servicos', function($table)
        {
            $table->dropColumn('online_service');
        });
    }
}
