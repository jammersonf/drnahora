<?php namespace Qualitare\DrNaHora\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class ChangeMedicosEspcialidadesField extends Migration
{
	public function up()
	{
		Schema::table('qualitare_drnahora_medicos', function(Blueprint $table) {
			$table->renameColumn('especialidades', 'especialidades_shosp');
		});
	}

	public function down()
	{
		Schema::table('qualitare_drnahora_medicos', function(Blueprint $table) {
			$table->renameColumn('especialidades_shosp', 'especialidades');
		});
	}
}
