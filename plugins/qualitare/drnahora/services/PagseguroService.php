<?php
namespace Qualitare\Api\Services;

use PagSeguro\Configuration\Configure;
use PagSeguro\Services\Session as PagseguroSession;
use PagSeguro\Domains\Requests\Payment as PagseguroPayment;

class PagseguroService {

    private $_configs;

    /**
     * Private Frenet constructor.
     * @param string $_apiUrl
     */
    public function __construct() {

        $this->_configs = new Configure();
        $this->_configs->setCharset('UTF-8');
        $this->_configs->setAccountCredentials(env('PAGSEGURO_EMAIL'), env('PAGSEGURO_TOKEN'));
        $this->_configs->setEnvironment(env('PAGSEGURO_ENVIRONMENT', 'sandbox'));
        //pode ser false
        $this->_configs->setLog(true, storage_path('logs/pagseguro_'. date('Ymd') .'.txt'));

    }

    public function getCredenciais() {
        return $this->_configs->getAccountCredentials();
    }

    public function createRequestPayment($payment_data) {
        try {

            $pagamento = new PagseguroPayment();
            $pagamento->setCurrency('BRL');

            $pagamento->setReference($payment_data['reference']);
            $pagamento->setRedirectUrl(env('APP_URL'));
            $pagamento->setExtraAmount($payment_data['extra_amount']);

            foreach ( $payment_data['items'] as $item ) {

                $pagamento->addItems()->withParameters(
                    $item['id'],
                    $item['description'],
                    $item['quantity'],
                    $item['amount'],
                    $item['weight']
                );

            }

            //pegar do usuario logado
            $pagamento->setSender()->setName($payment_data['sender']['name']);
            $pagamento->setSender()->setEmail($payment_data['sender']['email']);

//            $pagamento->setSender()->setDocument()->withParameters(
//                $payment_data['sender']['documents'][0]['type'],
//                $payment_data['sender']['documents'][0]['number']
//            );
//
//            //pegar telefone do usuario logado
//            $pagamento->setSender()->setPhone()->withParameters(
//                $payment_data['sender']['phone']['areaCode'],
//                $payment_data['sender']['phone']['number']
//            );
            $pagamento->setShipping()->setType()->withParameters(
                \PagSeguro\Enum\Shipping\Type::NOT_SPECIFIED
            );

            $pagamento->setShipping()->setAddress()->withParameters(
                $payment_data['shipping']['address']['street'],
                $payment_data['shipping']['address']['number'],
                $payment_data['shipping']['address']['district'],
                $payment_data['shipping']['address']['postalCode'],
                $payment_data['shipping']['address']['city'],
                $payment_data['shipping']['address']['state'],
                $payment_data['shipping']['address']['country']
            );
//
//            $pagamento->setShipping()->setCost()->withParameters($payment_data['shipping']['cost']);
//            $pagamento->setShipping()->setType()->withParameters($payment_data['shipping']['type']);

            $onlyCheckoutCode = true;
            $result = $pagamento->register($this->getCredenciais(),$onlyCheckoutCode);

            return $result->getCode();
        } catch (\Exception $e) {
            die($e->getMessage());
        }
    }

    public function getPaymentMethodType($type) {

        /**
         * 1	Cartão de crédito: o comprador escolheu pagar a transação com cartão de crédito.
         * 2	Boleto: o comprador optou por pagar com um boleto bancário.
         * 3	Débito online (TEF): o comprador optou por pagar a transação com débito online de algum dos bancos conveniados.
         * 4	Saldo PagSeguro: o comprador optou por pagar a transação utilizando o saldo de sua conta PagSeguro.
         * 5	Oi Paggo *: o comprador escolheu pagar sua transação através de seu celular Oi.
         */
        $types = [
            1 => 'Cartão de crédito',
            2 => 'Boleto',
            3 => 'Débito online (TEF)',
            4 => 'Saldo PagSeguro',
            5 => 'Oi Paggo*',
        ];

        return $types[$type];

    }

    public function getPaymentMethodCode($code) {
        /**
         * 101	Cartão de crédito Visa.
         * 102	Cartão de crédito MasterCard.
         * 103	Cartão de crédito American Express.
         * 104	Cartão de crédito Diners.
         * 105	Cartão de crédito Hipercard.
         * 106	Cartão de crédito Aura.
         * 107	Cartão de crédito Elo.
         * 108	Cartão de crédito PLENOCard.
         * 109	Cartão de crédito PersonalCard.
         * 110	Cartão de crédito JCB.
         * 111	Cartão de crédito Discover.
         * 112	Cartão de crédito BrasilCard.
         * 113	Cartão de crédito FORTBRASIL.
         * 114	Cartão de crédito CARDBAN.
         * 115	Cartão de crédito VALECARD.
         * 116	Cartão de crédito Cabal.
         * 117	Cartão de crédito Mais!.
         * 118	Cartão de crédito Avista.
         * 119	Cartão de crédito GRANDCARD.
         * 120	Cartão de crédito Sorocred.
         * 122	Cartão de crédito Up Policard.
         * 123	Cartão de crédito Banese Card.
         * 201	Boleto Bradesco.
         * 202	Boleto Santander.
         * 301	Débito online Bradesco.
         * 302	Débito online Itaú.
         * 303	Débito online Unibanco.
         * 304	Débito online Banco do Brasil.
         * 305	Débito online Banco Real.
         * 306	Débito online Banrisul.
         * 307	Débito online HSBC.
         * 401	Saldo PagSeguro.
         * 501	Oi Paggo. *
         * 701	Depósito em conta - Banco do Brasil
         */

        $codes = [
            101	=> 'Cartão de crédito Visa',
            102	=> 'Cartão de crédito MasterCard',
            103	=> 'Cartão de crédito American Express',
            104	=> 'Cartão de crédito Diners',
            105	=> 'Cartão de crédito Hipercard',
            106	=> 'Cartão de crédito Aura',
            107	=> 'Cartão de crédito Elo',
            108	=> 'Cartão de crédito PLENOCard',
            109	=> 'Cartão de crédito PersonalCard',
            110	=> 'Cartão de crédito JCB',
            111	=> 'Cartão de crédito Discover',
            112	=> 'Cartão de crédito BrasilCard',
            113	=> 'Cartão de crédito FORTBRASIL',
            114	=> 'Cartão de crédito CARDBAN',
            115	=> 'Cartão de crédito VALECARD',
            116	=> 'Cartão de crédito Cabal',
            117	=> 'Cartão de crédito Mais!',
            118	=> 'Cartão de crédito Avista',
            119	=> 'Cartão de crédito GRANDCARD',
            120	=> 'Cartão de crédito Sorocred',
            122	=> 'Cartão de crédito Up Policard',
            123	=> 'Cartão de crédito Banese Card',
            201	=> 'Boleto Bradesco',
            202	=> 'Boleto Santander',
            301	=> 'Débito online Bradesco',
            302	=> 'Débito online Itaú',
            303	=> 'Débito online Unibanco',
            304	=> 'Débito online Banco do Brasil',
            305	=> 'Débito online Banco Real',
            306	=> 'Débito online Banrisul',
            307	=> 'Débito online HSBC',
            401	=> 'Saldo PagSeguro',
            501	=> 'Oi Paggo. ',
            701	=> 'Depósito em conta - Banco do Brasi',
        ];

        return $codes[$code];

    }

}
